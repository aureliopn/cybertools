# CyberTools

Dockerfile to build your Docker image containing Cybersecurity tools.

## Included tools

Installed tools:

* Mapcidr.
* Dnsx.
* Murmurhash.
* DNSvalidator.
* Massdns.
* Puredns.
* Gotator.
* Analytics Relationships.
* Cero.
* Gospider.
* Unfurl.
* IP range to CIDR.
* Waybackurl.
* Github-Search.
* Amass.
* Sublist3r.
* Spiderfoot.
* Httpx.
* Eyewitness.
* Gobuster.
* Wappalyzer.
* Nuclei.
* Spoofcheck.
* Subzy.
* Exiftool.
* Infoga.
* Sherlock.
* Facebook-scraper.
* Instagram-scraper.
* OSRFramework.
* Eagle-Eye.
* OnionSearch.

## Custom image building

```bash
git clone https://gitlab.com/aureliopn/cybertools
cd cybertools
docker build -t cybertools:latest .
```

## Tools usage examples

### Spoofcheck

```bash
docker run --rm ureure/cybertools:latest python3 /tools/spoofcheck/spoofcheck.py [DOMAIN]
```

### Sherlock

```bash
docker run --rm ureure/cybertools:latest python3 /tools/sherlock/sherlock [user1]
```
