FROM golang:1.17.6

RUN apt-get update && apt-get install python3-pip -y

WORKDIR /tools

# Mapcidr
RUN go install github.com/projectdiscovery/mapcidr/cmd/mapcidr@latest

# Dnsx
RUN go install github.com/projectdiscovery/dnsx/cmd/dnsx@latest

# Murmurhash
RUN git clone https://github.com/UnaPibaGeek/ctfr.git && cd ctfr && pip3 install -r requirements.txt && cd ..

# DNSvalidator
RUN git clone https://github.com/vortexau/dnsvalidator.git && cd dnsvalidator/ && python3 setup.py install && cd ..

# Massdns
RUN git clone https://github.com/blechschmidt/massdns.git && cd massdns && make && make install && cd ..

# Puredns
RUN go install github.com/d3mondev/puredns/v2@latest

# Gotator
RUN git clone https://github.com/Josue87/gotator.git && cd gotator && go build && go install && cd ..

# Analytics Relationships
RUN git clone https://github.com/Josue87/AnalyticsRelationships.git && cd AnalyticsRelationships/ && go build -ldflags "-s -w" && go install && cd ..

# Cero
RUN go install github.com/glebarez/cero@latest

# Gospider
RUN go install github.com/jaeles-project/gospider@latest

# Unfurl
RUN go install github.com/tomnomnom/unfurl@latest

# IP range to CIDR
RUN go install github.com/raspi/ip-range-to-CIDR@latest

# Waybackurl
RUN go install github.com/tomnomnom/waybackurls@latest

# Github-Search
RUN git clone https://github.com/gwen001/github-search && cd github-search && pip3 install -r requirements2.txt && cd ..

# Amass
RUN go install -v github.com/OWASP/Amass/v3/...@master

# Sublist3r
RUN git clone https://github.com/aboul3la/Sublist3r.git && cd Sublist3r && pip3 install -r requirements.txt && cd ..

# Spiderfoot
RUN wget https://github.com/smicallef/spiderfoot/archive/v3.5.tar.gz && tar zxvf v3.5.tar.gz && cd spiderfoot-3.5 && pip3 install -r requirements.txt && cd .. && rm v3.5.tar.gz

# Httpx
RUN go install -v github.com/projectdiscovery/httpx/cmd/httpx@latest

# Eyewitness
RUN git clone https://github.com/FortyNorthSecurity/EyeWitness && cd EyeWitness/Python/setup && ./setup.sh && cd ../../..

# Gobuster
RUN go install github.com/OJ/gobuster/v3@latest

# Wappalyzer
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - && echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list && apt-get update && apt-get install yarn -y && git clone https://github.com/AliasIO/wappalyzer.git && cd wappalyzer && yarn install && yarn run link && cd ..

# Nuclei
RUN go install -v github.com/projectdiscovery/nuclei/v2/cmd/nuclei@latest

# Spoofcheck
RUN git clone https://github.com/a6avind/spoofcheck && cd spoofcheck && pip3 install -r requirements.txt && cd ..

# Subzy
RUN go install -v github.com/lukasikic/subzy@latest

# Exiftool
RUN apt-get install libimage-exiftool-perl -y

# Infoga
RUN git clone https://github.com/m4ll0k/Infoga.git && cd Infoga && python3 setup.py install && cd ..

# Sherlock
RUN git clone https://github.com/sherlock-project/sherlock.git && cd sherlock && python3 -m pip install -r requirements.txt && cd ..

# Facebook-scraper
RUN pip3 install facebook-scraper

# Instagram-scraper
RUN pip3 install instagram-scraper

# OSRFramework
RUN git clone https://github.com/i3visio/osrframework && cd osrframework && pip3 install -r requirements.txt && pip3 install . && cd ..

# Eagle-Eye
RUN apt-get install cmake -y && git clone https://github.com/ThoughtfulDev/EagleEye && cd EagleEye && pip3 install --upgrade pylnk3 && pip3 install --upgrade neo4j && pip3 install -r requirements.txt && pip3 install --upgrade beautifulsoup4 html5lib spry

# OnionSearch
RUN git clone https://github.com/megadose/OnionSearch.git && cd OnionSearch && python3 setup.py install && cd ..
